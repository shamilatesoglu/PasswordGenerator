/**
 *  Copyright (C) 2016 Samil Atesoglu & Abdullah Coban
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package msa.tools.passwordgenerator;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Stream;
import javax.imageio.ImageIO;

/**
 *
 * @author Homeless Man
 */
public final class PasswordGenerator extends javax.swing.JFrame {

    int xMouse;
    int yMouse;

    public static final Color POPPYRED = new Color(189, 42, 78);
    public static final Color MIGHTY_SLATE = new Color(85, 98, 112);
    public static final Color BLACK = new Color(0, 0, 0);
    public static final Color TITLE_COLOR = new Color(27, 31, 35);
    public static final Color CLOSE_COLOR = new Color(255, 0, 51);

    String[] lowercaseArr = new String[26];
    String[] uppercaseArr = new String[26];
    String[] symbolArr = new String[15];
    String[] numberArr = new String[10];
    String[] otherArr = new String[18];

    public PasswordGenerator() {
        initComponents();
        gatherArrays();
        initEnv();
        KeyListener listener = new ListenKeyboard();
        addKeyListener(listener);
        innerPanel.addKeyListener(listener);
        mainPanel.addKeyListener(listener);
        titleLabel.addKeyListener(listener);
        generateB.addKeyListener(listener);
        passwordLength.addKeyListener(listener);
        password.addKeyListener(listener);
        passwordPanel.addKeyListener(listener);
        setFocusable(true);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);
    }

    public void initEnv() {
//        try {
//            InputStream imgStream = PasswordGenerator.class.getResourceAsStream("icon32.png");
//            BufferedImage iconImg = ImageIO.read(imgStream);
//            this.setIconImage(iconImg);
//        } catch (IOException ex) {
//        }
    }

    public class ListenKeyboard implements KeyListener {

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                generateB.doClick();
            }
            if (e.getKeyCode() == KeyEvent.VK_C) {
                password.selectAll();
                password.copy();
            }
            if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                generateB.doClick();
            }
            if (e.getKeyCode() == KeyEvent.VK_1) {
                includeSymbols.doClick();
            }
            if (e.getKeyCode() == KeyEvent.VK_2) {
                includeNumbers.doClick();
            }
            if (e.getKeyCode() == KeyEvent.VK_3) {
                includeLowercase.doClick();
            }
            if (e.getKeyCode() == KeyEvent.VK_4) {
                includeUppercase.doClick();
            }
            if (e.getKeyCode() == KeyEvent.VK_5) {
                includeOther.doClick();
            }
        }

        @Override
        public void keyReleased(KeyEvent e) {

        }
    }

    public void gatherArrays() {
        this.lowercaseArr = new String[]{
            "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n",
            "o", "p", "r", "s", "t", "u", "v", "w", "x", "y", "z" //abcdefghijklmnopqrstuvwxyz
        };
        this.uppercaseArr = new String[]{
            "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N",
            "O", "P", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"
        };
        this.numberArr = new String[]{
            "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"
        };
        this.symbolArr = new String[]{
            "@", "£", "!", "^", "#", "&", "*", "?", "€", "-", "+", "|", "=", "_", "%"
        };
        this.otherArr = new String[]{
            "{", "}", "[", "]", "(", ")", "\\", "/", "\'", "\"", "`", "~", ",",
            ";", ":", ".", "<", ">"
        };
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainPanel = new javax.swing.JPanel();
        innerPanel = new javax.swing.JPanel();
        includeSymbols = new javax.swing.JCheckBox();
        includeNumbers = new javax.swing.JCheckBox();
        includeLowercase = new javax.swing.JCheckBox();
        includeUppercase = new javax.swing.JCheckBox();
        passwordLength = new javax.swing.JComboBox<>();
        lL = new javax.swing.JLabel();
        includeOther = new javax.swing.JCheckBox();
        passwordPanel = new javax.swing.JPanel();
        password = new javax.swing.JTextField();
        generateB = new javax.swing.JButton();
        //iconL = new javax.swing.JLabel();
        titleLabel = new javax.swing.JLabel();
        closeB = new javax.swing.JButton();
        bL = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("THAT PASSWORD...");
        setUndecorated(true);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        mainPanel.setBackground(new java.awt.Color(27, 31, 35));
        mainPanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        mainPanel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        innerPanel.setBackground(new java.awt.Color(85, 98, 112));
        innerPanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        innerPanel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        includeSymbols.setFont(new java.awt.Font("ITC Avant Garde Gothic", 0, 14)); // NOI18N
        includeSymbols.setForeground(new java.awt.Color(255, 255, 255));
        includeSymbols.setText("Include Symbols  (@, #, $, %, &, *, €, ^, +, -, ! etc...)");
        includeSymbols.setOpaque(false);
        innerPanel.add(includeSymbols, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 80, -1, 30));

        includeNumbers.setFont(new java.awt.Font("ITC Avant Garde Gothic", 0, 14)); // NOI18N
        includeNumbers.setForeground(new java.awt.Color(255, 255, 255));
        includeNumbers.setText("Include Numbers (0, 1, 2, 3, 4, 5, 6, 7, 8, 9)");
        includeNumbers.setToolTipText("Yes! I can count 0 to 9!");
        includeNumbers.setOpaque(false);
        innerPanel.add(includeNumbers, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 110, -1, 30));

        includeLowercase.setFont(new java.awt.Font("ITC Avant Garde Gothic", 0, 14)); // NOI18N
        includeLowercase.setForeground(new java.awt.Color(255, 255, 255));
        includeLowercase.setText("Include Lowercase Characters (a, b, c, d, e, f, g, h...)");
        includeLowercase.setOpaque(false);
        innerPanel.add(includeLowercase, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 140, -1, 30));

        includeUppercase.setFont(new java.awt.Font("ITC Avant Garde Gothic", 0, 14)); // NOI18N
        includeUppercase.setForeground(new java.awt.Color(255, 255, 255));
        includeUppercase.setText("Include Uppercase Characters (A, B, C, D, E, F, G, H...)");
        includeUppercase.setOpaque(false);
        innerPanel.add(includeUppercase, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 170, -1, 30));

        passwordLength.setBackground(new java.awt.Color(114, 135, 150));
        passwordLength.setFont(new java.awt.Font("ITC Avant Garde Gothic Demi", 1, 14)); // NOI18N
        passwordLength.setForeground(new java.awt.Color(255, 255, 255));
        passwordLength.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Password Length", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50" }));
        passwordLength.setSelectedIndex(16);
        innerPanel.add(passwordLength, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 40, 410, -1));

        lL.setFont(new java.awt.Font("ITC Avant Garde Gothic Demi", 1, 14)); // NOI18N
        lL.setForeground(new java.awt.Color(255, 255, 255));
        lL.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lL.setText("Password Length");
        innerPanel.add(lL, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 410, 30));

        includeOther.setFont(new java.awt.Font("ITC Avant Garde Gothic", 0, 14)); // NOI18N
        includeOther.setForeground(new java.awt.Color(255, 255, 255));
        includeOther.setText("Include; ( { }  [ ] ( ) / \\ ' \" ` ~ , ; : . < >)");
        includeOther.setOpaque(false);
        innerPanel.add(includeOther, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 200, -1, 30));

        passwordPanel.setBackground(new java.awt.Color(85, 98, 112));
        passwordPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 3), "Randomly Generated Password", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("ITC Avant Garde Gothic", 0, 16), new java.awt.Color(255, 255, 255))); // NOI18N
        passwordPanel.setForeground(new java.awt.Color(255, 255, 255));
        passwordPanel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        password.setBackground(new java.awt.Color(59, 67, 76));
        password.setFont(new java.awt.Font("ITC Avant Garde Gothic", 0, 14)); // NOI18N
        password.setForeground(new java.awt.Color(255, 255, 255));
        password.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        password.setToolTipText("Press 'C' to copy");
        password.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 2));
        passwordPanel.add(password, new org.netbeans.lib.awtextra.AbsoluteConstraints(19, 34, 370, 27));

        innerPanel.add(passwordPanel, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 280, 410, 80));

        generateB.setBackground(new java.awt.Color(114, 135, 150));
        generateB.setFont(new java.awt.Font("ITC Avant Garde Gothic", 1, 14)); // NOI18N
        generateB.setForeground(new java.awt.Color(255, 255, 255));
        generateB.setText("Generate");
        generateB.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 3));
        generateB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                generateBActionPerformed(evt);
            }
        });
        innerPanel.add(generateB, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 236, 410, 30));

//        iconL.setBackground(new java.awt.Color(59, 67, 76));
//        iconL.setFont(new java.awt.Font("ITC Avant Garde Gothic Demi", 1, 14)); // NOI18N
//        iconL.setForeground(new java.awt.Color(255, 255, 255));
//        iconL.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
//        //iconL.setIcon(new javax.swing.ImageIcon(getClass().getResource("/password/generator/hmsIcon.png"))); // NOI18N
//        iconL.setToolTipText("Homeless Man (Developed in Turkey)");
//        iconL.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
//        iconL.setOpaque(true);
//        innerPanel.add(iconL, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 10, -1, -1));
//
        mainPanel.add(innerPanel, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 40, 430, 370));

        titleLabel.setBackground(new java.awt.Color(27, 31, 35));
        titleLabel.setFont(new java.awt.Font("ITC Avant Garde Gothic Demi", 1, 16)); // NOI18N
        titleLabel.setForeground(new java.awt.Color(255, 255, 255));
//        titleLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/password/generator/icon16.png"))); // NOI18N
        titleLabel.setText(" PASSWORD GENERATOR");
        titleLabel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        titleLabel.setIconTextGap(0);
        titleLabel.setOpaque(true);
        titleLabel.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                titleLabelMouseDragged(evt);
            }
        });
        titleLabel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                titleLabelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                titleLabelMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                titleLabelMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                titleLabelMouseReleased(evt);
            }
        });
        mainPanel.add(titleLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 390, 30));

        closeB.setBackground(new java.awt.Color(255, 0, 51));
        closeB.setFont(new java.awt.Font("ITC Avant Garde Gothic Demi", 1, 14)); // NOI18N
        closeB.setForeground(new java.awt.Color(255, 255, 255));
        closeB.setText("X");
        closeB.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        closeB.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                closeBMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                closeBMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                closeBMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                closeBMouseReleased(evt);
            }
        });
        closeB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                closeBActionPerformed(evt);
            }
        });
        mainPanel.add(closeB, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 0, 60, 30));

        bL.setFont(new java.awt.Font("Consolas", 0, 11)); // NOI18N
        bL.setForeground(new java.awt.Color(255, 255, 255));
        bL.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        bL.setText("Copyright 2016 Samil Atesoglu");
        bL.setToolTipText("Copyright 2016 Samil Atesoglu");
        mainPanel.add(bL, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 410, 430, 30));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mainPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mainPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void titleLabelMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_titleLabelMouseEntered
        titleLabel.setBackground(MIGHTY_SLATE);
    }//GEN-LAST:event_titleLabelMouseEntered

    private void titleLabelMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_titleLabelMouseExited
        titleLabel.setBackground(TITLE_COLOR);
    }//GEN-LAST:event_titleLabelMouseExited

    private void titleLabelMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_titleLabelMouseDragged
        int x = evt.getXOnScreen();
        int y = evt.getYOnScreen();
        this.setLocation(x - xMouse, y - yMouse);
    }//GEN-LAST:event_titleLabelMouseDragged

    private void titleLabelMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_titleLabelMousePressed
        xMouse = evt.getX();
        yMouse = evt.getY();
        titleLabel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
    }//GEN-LAST:event_titleLabelMousePressed

    private void closeBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_closeBActionPerformed
        System.exit(0);
    }//GEN-LAST:event_closeBActionPerformed

    private void closeBMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_closeBMouseEntered
        closeB.setBackground(POPPYRED);
    }//GEN-LAST:event_closeBMouseEntered

    private void closeBMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_closeBMouseExited
        closeB.setBackground(CLOSE_COLOR);
    }//GEN-LAST:event_closeBMouseExited

    private void closeBMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_closeBMousePressed
        closeB.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
    }//GEN-LAST:event_closeBMousePressed

    private void closeBMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_closeBMouseReleased
        closeB.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    }//GEN-LAST:event_closeBMouseReleased

    private void titleLabelMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_titleLabelMouseReleased
        titleLabel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    }//GEN-LAST:event_titleLabelMouseReleased

    private void generateBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_generateBActionPerformed
        password.setText("");
        String s = "";
        if (passwordLength.getSelectedIndex() != 0) {
            for (int i = 0; i < passwordLength.getSelectedIndex(); i++) {
                if (includeSymbols.isSelected() == true
                        | includeNumbers.isSelected() == true | includeLowercase.isSelected() == true
                        | includeUppercase.isSelected() == true | includeOther.isSelected() == true) {
                    String[] totalArr = new String[0];
                    if (includeSymbols.isSelected() == true) {
                        totalArr = Stream.of(totalArr, symbolArr).flatMap(Stream::of).toArray(String[]::new);
                    }
                    if (includeNumbers.isSelected() == true) {
                        totalArr = Stream.of(totalArr, numberArr).flatMap(Stream::of).toArray(String[]::new);
                    }
                    if (includeLowercase.isSelected() == true) {
                        totalArr = Stream.of(totalArr, lowercaseArr).flatMap(Stream::of).toArray(String[]::new);
                    }
                    if (includeUppercase.isSelected() == true) {
                        totalArr = Stream.of(totalArr, uppercaseArr).flatMap(Stream::of).toArray(String[]::new);
                    }
                    if (includeOther.isSelected() == true) {
                        totalArr = Stream.of(totalArr, otherArr).flatMap(Stream::of).toArray(String[]::new);
                    }
                    int rnd = ThreadLocalRandom.current().nextInt(0, totalArr.length);
                    s += totalArr[rnd];
                    password.setText(s);
                }
            }
        }
    }//GEN-LAST:event_generateBActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        bL.setText("Password Generator v1.0 © 2016 Samil Atesoglu");
    }//GEN-LAST:event_formWindowOpened

    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Metal".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PasswordGenerator.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        java.awt.EventQueue.invokeLater(() -> {
            new PasswordGenerator().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel bL;
    private javax.swing.JButton closeB;
    private javax.swing.JButton generateB;
    //private javax.swing.JLabel iconL;
    private javax.swing.JCheckBox includeLowercase;
    private javax.swing.JCheckBox includeNumbers;
    private javax.swing.JCheckBox includeOther;
    private javax.swing.JCheckBox includeSymbols;
    private javax.swing.JCheckBox includeUppercase;
    private javax.swing.JPanel innerPanel;
    private javax.swing.JLabel lL;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JTextField password;
    private javax.swing.JComboBox<String> passwordLength;
    private javax.swing.JPanel passwordPanel;
    private javax.swing.JLabel titleLabel;
    // End of variables declaration//GEN-END:variables
}
